<?php

use Illuminate\Http\Request;

use App\Tramite;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');
Route::post('register', 'Auth\RegisterController@register');

//authenticated routes
Route::group(['middleware' => 'auth:api'], function() {
    Route::post('documento', 'API\DocumentoController@store');
    Route::put('documento/{documento}', 'API\DocumentoController@update');
    Route::delete('documento/{documento}', 'API\DocumentoController@destroy');
    Route::post('paso', 'API\PasoController@store');
    Route::put('paso/{paso}', 'API\PasoController@update');
    Route::delete('paso/{paso}', 'API\PasoController@destroy');
    Route::post('tramite', 'API\TramiteController@store');
    Route::put('tramite/{tramite}', 'API\TramiteController@update');
    Route::delete('tramite/{tramite}', 'API\TramiteController@destroy');
});

// Routes for tramite
Route::get('tramite', 'API\TramiteController@index');
Route::get('tramite/{tramite}', 'API\TramiteController@show');
Route::get('tramite/{tramite}/pasos', 'API\TramiteController@pasos');

// Routes for tramite
Route::get('paso', 'API\PasoController@index');
Route::get('paso/{paso}', 'API\PasoController@show');

// Routes for documento
Route::get('documento', 'API\DocumentoController@index');
Route::get('documento/{documento}', 'API\DocumentoController@show');