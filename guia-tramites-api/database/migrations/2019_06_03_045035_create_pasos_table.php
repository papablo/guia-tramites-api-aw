<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasos', function (Blueprint $table) {
            $table->integer('tramite_id');

            $table->foreign('tramite_id')->references('id')->on('tramites')->onDelete('cascade');

            $table->bigIncrements('id');
            $table->integer('orden');
            $table->string('nombre');
            $table->string('descripcion');

            $table->unique(['tramite_id','orden']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasos');
    }
}
