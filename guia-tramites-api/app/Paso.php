<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paso extends Model
{
    //
    protected $fillable=['nombre', 'descripcion','orden','tramite_id'];
}
