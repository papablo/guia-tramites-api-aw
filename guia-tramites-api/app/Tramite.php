<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tramite extends Model
{
    // Model for tramite

    protected $fillable = ['titulo', 'descripcion'];
    
}
