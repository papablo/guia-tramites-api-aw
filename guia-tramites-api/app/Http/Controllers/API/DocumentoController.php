<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Documento;

use Illuminate\Support\Facades\Storage;

class DocumentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Documento::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $documento = new Documento;

            $documento->nombre = $request->nombre;
            $documento->link = '/api/documento/'.$request->nombre;
            $documento->paso_id = $request->paso_id;

            $documento->save();

            $request->file('documento')->storeAs('documentos',$request->nombre);

            return $documento;
        } catch (\Throwable $th) {
            return response(['data'=>$th],400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($documento)
    {
        try {
            return Storage::download('documentos/'.$documento);
        } catch (\Throwable $th) {
            echo $th;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $documento)
    {
        $request->file('documento')->storeAs('documentos',$documento);

        return response(['data'=>'updated'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Documento $documento)
    {
        $documento->delete();

        return response(null, 204);
    }
}
