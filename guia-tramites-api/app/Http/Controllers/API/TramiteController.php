<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Tramite;
use App\Paso;
use App\Documento;

class TramiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Tramite::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tramite = Tramite::create($request->all());

        return response()->json($tramite,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tramite $tramite)
    {

        return $tramite;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tramite $tramite)
    {
        $tramite->update($request->all());

        return response($tramite, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Tramite  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tramite $tramite)
    {
        $tramite->delete();

        return response(null, 204);
    }

    /**
     * Return the pasos associated with a certain tramite
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function pasos(Tramite $tramite)
    {

    
        $pasos = Paso::where('tramite_id', $tramite->id)->get();

        $pasos->map(function($paso) {

            $documento = Documento::where('paso_id', $paso->id)->get();
            $paso->documento = $documento;

            return $paso;
        });

        return $pasos;

    }

}
