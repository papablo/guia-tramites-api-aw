FROM php:7
RUN apt-get update -y && apt-get install -y openssl zip unzip git zlib1g-dev libzip-dev libpq-dev
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install zip pdo mbstring pgsql pdo_pgsql

WORKDIR /app

COPY guia-tramites-api/ /app

RUN composer install 

RUN chmod +x /app/entrypoint.sh

expose 8000


